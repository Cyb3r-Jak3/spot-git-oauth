"""App For Flask/Gunicorn"""
from server.main import app as application

if __name__ == "__main__":
    application.run()
