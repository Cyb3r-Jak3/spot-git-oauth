"""Main Flask Setup"""
from datetime import datetime
import os
from flask import Flask, render_template, request, jsonify

from server.oauth import access, refresh, TokenError, VERSION

app = Flask("Spot_Git_OAuth")


@app.route("/", methods=["GET"])
def home_view():
    """HomePage view with basic info"""
    return render_template("base.jinja")


@app.route("/ping", methods=["GET"])
def ping():
    """Used for Health Check and status"""
    return jsonify(
        {
            "VERSION": VERSION,
            "GIT_HASH": os.getenv("HEROKU_SLUG_COMMIT"),
            "RELEASE_DATE": os.getenv(
                "HEROKU_RELEASE_CREATED_AT",
            ),
        }
    )


@app.route("/token", methods=["GET"])
def code_view():
    """Route for getting a token from a code"""
    return render_template(
        "token.jinja",
        date=datetime.utcnow().strftime("%m/%d/%Y, %H:%M:%S"),
        full=access(request.args.get("code"), False),
    )


@app.route("/api/token", methods=["POST"])
def api_token():
    """Allows for API Tokens Acquisitions"""
    body = request.get_json()
    if body is None:
        return "Body was not sent as json", 400
    return jsonify({"token": access(body["code"], True)})


@app.route("/api/refresh", methods=["POST"])
def api_refresh():
    """Allows for API Token refresh"""
    body = request.get_json()
    if body is None:
        return "Body was not sent as json", 400
    return jsonify({"token": refresh(body["token"], True)})


@app.errorhandler(TokenError)
def bad_token(err):
    """
    Error handling for TokenError

    Parameters
    ----------
    err: TokenError
    """
    if err.api:
        return err.message, 500
    return render_template("error.jinja", error=err.message)
