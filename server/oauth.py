""" Module that allows for management of spotify playlists"""
import base64
import os
import secrets
import string
import time
import requests

VERSION = "0.0.1.dev1"

# Bandit throws warning about possible hardcoded password.
TOKEN_URL = "https://accounts.spotify.com/api/token"  # nosec: B105


class TokenError(Exception):
    """Error that gets raise if the Token fails"""

    def __init__(self, full_error: dict, api: bool):
        """
        Parameters
        ----------
        full_error: dict
            The full error JSON from Spotify
        api: bool
            If the call was made from an API endpoint
        """
        self.message = full_error
        self.api = api
        super().__init__()


def state_gen(string_length: int) -> str:
    """
    Makes a random string that is used for the state.
    Parameters
    ----------
    string_length: int
        Length of state string to use

    Returns
    -------
        Securely generated string of ``string_length`` length
    """
    characters = string.ascii_letters + string.digits
    return "".join(secrets.choice(characters) for _ in range(string_length))


def make_authorization_headers() -> dict:
    """
    Base64 encodes the client id and secret for the authorization headers
    Returns
    -------
        A dict of headers that are used to make the token requests
    """
    return {
        "User-Agent": f"Spot-Git-OAuth v:{VERSION}",
        "Authorization": "Basic {}".format(
            base64.b64encode(
                (
                    os.environ["CLIENT_ID"] + ":" + os.environ["CLIENT_SECRET"]
                ).encode("utf-8")
            ).decode("utf-8")
        ),
    }


def add_expiration(token: dict) -> dict:
    """
    Adds an expiration time for the token
    Parameters
    ----------
    token: dict
        The Spotify Token

    Returns
    -------
        The token with an added ``expires`` key
    """
    token["expires"] = int(time.time()) + token["expires_in"]
    return token


def check_expiration(token: dict) -> bool:
    """
    Checks to see if the access token has expired
    Parameters
    ----------
    token: dict
        The Spotify Token that is being checked

    Returns
    -------
        False is the token is expired.
    """
    return token["expires"] - int(time.time()) < 60


def access(code: str, api: bool) -> dict:
    """
    Gets the access token from spotify

    Raises
    ------
        TokenError: If there is an issue getting the token from Spotify

    Parameters
    ----------
    code: str
        The code the is returned from Spotify to use get a token
    api: bool
        If the request came the from the API endpoint or normal

    Returns
    -------
        A dictionary of the Spotify Token
    """
    headers = make_authorization_headers()
    payload = {
        "redirect_uri": os.getenv(
            "REDIRECT_URL", "https://spot-git-prod.herokuapp.com/token"
        ),
        "code": code,
        "grant_type": "authorization_code",
    }
    resp = requests.post(
        TOKEN_URL, data=payload, headers=headers, verify=True
    ).json()
    if "error" in resp:
        raise TokenError(resp, api)
    token = add_expiration(resp)
    return token


def refresh(token: dict, api: bool) -> dict:
    """
    Gets an access token using the refresh token
    Parameters
    ----------
    token: str
        The refresh token
    api: bool
        If the request came the from the API endpoint or normal

    Returns
    -------
        Dictionary of a new access token
    """
    headers = make_authorization_headers()
    payload = {
        "refresh_token": token,
        "grant_type": "refresh_token",
    }
    resp = requests.post(TOKEN_URL, data=payload, headers=headers).json()
    if "error" in resp:
        raise TokenError(resp, api)
    new_token = add_expiration(resp)
    return new_token
