"""Used for testing"""
import secrets
import string
import os
import webbrowser
import requests


REDIRECT_URL = os.getenv("REDIRECT_URL", "https://spot-git.jwhite.network/")


def state_gen(string_length: int):
    """Makes a random string that is used for the state"""
    characters = string.ascii_letters + string.digits
    return ''.join(secrets.choice(characters) for i in range(string_length))


def login(dialog: str = "true"):
    """Gets the login url and opens it"""
    state = state_gen(20)
    base_login_url = "https://accounts.spotify.com/authorize?"
    login_url = (base_login_url + "client_id=" + os.environ["CLIENT_ID"]
                 + "&response_type=code"
                 + f"&redirect_uri={REDIRECT_URL}"
                 + "&scope=playlist-modify-private%20playlist-read-private%20"
                   "playlist-modify-public%20playlist-read-collaborative"
                 + "&show_dialog={}".format(dialog)
                 + "&state={}".format(state))
    resp = requests.get(login_url)
    webbrowser.open_new_tab(resp.url)


if __name__ == "__main__":
    login()
