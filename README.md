Spot-Git OAuth
---

![](logos/SpotGit_Logo_small.png)

This is the OAuth server that is used by my program [SpotGit](https://gitlab.com/Cyb3r-Jak3/spotify-git).

## Endpoints

There are 4 endpoints that are currently used

### /token

Used to generate a Spotify OAuth token with a web page 

### /api/token

Used to generate a Spotify OAuth token through REST API

### /api/refresh

Used to refresh a Spotify OAuth token through REST API

### /ping

Used to show stats like current git hash, release date and version