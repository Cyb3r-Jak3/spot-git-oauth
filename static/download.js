function download() {
    var element = document.createElement('a');
    console.log(document.getElementById('token').innerHTML)
    element.setAttribute('href', 'data:text/json;charset=utf-8,' + escape(JSON.stringify(document.getElementById('token').innerHTML)));
    element.setAttribute('download', "token.json");

    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
}